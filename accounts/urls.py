from django.contrib import admin
from django.urls import path

from accounts.views import *


urlpatterns = [
    path('', login_view),
    path('register/', register_view),
    path('logout/', logout_view)
]

