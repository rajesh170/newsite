from django.shortcuts import render
from .models import News
from django.views.generic import *
from django.http import HttpResponseRedirect



class NewsListView(ListView):
    model = News
    template_name = 'index.html'
    queryset = News.objects.all().order_by("-id")
    context_object_name = "allnews"


class DetailListView(DeleteView):
    template_name='detail.html'
    model = News
    context_object_name='alldetail'

