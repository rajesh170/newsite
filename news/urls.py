from django.contrib import admin
from django.urls import path
from news.views import *
from django.views.generic import *

urlpatterns = [
    path('',NewsListView.as_view()),
    path('home/',NewsListView.as_view()),
    path('<int:pk>/detail/',DetailListView.as_view()),
     
    
 ]
