from django.shortcuts import render

from .models import International
from django.views.generic import ListView


class InternationalView(ListView):
    model = International
    template_name = 'international.html'
    queryset = International.objects.all().order_by("-id")
    context_object_name = "allInternational"


