from django.contrib import admin
from django.urls import path

from international.views import InternationalView
from django.views.generic import ListView

urlpatterns = [
    path('',InternationalView.as_view()),
 ]
