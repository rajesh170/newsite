from django.shortcuts import render
from .models import Politics
from django.views.generic import ListView

class PoliticsView(ListView):
    model = Politics
    template_name = 'politics.html'
    queryset = Politics.objects.all().order_by("-id")
    context_object_name = "allpolitics"


