from django.contrib import admin
from django.urls import path
from politics.views import PoliticsView
from django.views.generic import ListView

urlpatterns = [
    path('politics/',PoliticsView.as_view()),
 ]
