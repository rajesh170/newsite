from django.db import models

class Politics(models.Model):
    title           =   models.CharField(max_length=150)
    Description     =   models.TextField(blank=True)
    images          =   models.ImageField(upload_to='images')
    date            =   models.DateTimeField(auto_now_add=True)